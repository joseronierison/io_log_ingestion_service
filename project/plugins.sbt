resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
resolvers += Classpaths.sbtPluginReleases

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.18")

addSbtPlugin("io.gatling" % "gatling-sbt" % "2.2.1")

addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "0.3.1")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

addSbtPlugin("com.waioeka.sbt" % "cucumber-plugin" % "0.1.6")
