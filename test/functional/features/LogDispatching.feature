Feature: Send Logs to Ingestion Queue
  In order to have the devices logs saved
  As a Vision Customer
  I want to send a log to the io_log_service endpoint so it must send it to the ingestion queue

  Scenario: A JSON string is sent from device
    Given The service is up and running
    When I receive a log in Json format
    Then I should send a log event to ingestion exchange