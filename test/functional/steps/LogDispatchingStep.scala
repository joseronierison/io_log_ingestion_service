package functional.steps

import com.typesafe.config.ConfigFactory
import common.QueueHelper
import common.builders.IOLogPayloadBuilder
import config.RabbitMQConfig
import cucumber.api.scala.{EN, ScalaDsl}
import domain.LogEvent
import infrastructure.parsing.LogEventReadersAndWriters
import org.scalatest.Matchers
import org.scalatest.concurrent.Eventually._
import play.api.libs.json.Json
import scalaj.http._
import scala.concurrent.duration._
import scala.language.postfixOps

class LogDispatchingStep extends ScalaDsl with Matchers with EN with LogEventReadersAndWriters {
  private val configLoad = ConfigFactory.load()
  private val IO_LOG_CONFIG_HOST = "io_log_ingestion_service.host"
  private val IO_LOG_CONFIG_PORT = "io_log_ingestion_service.port"
  private val ioLogHost = configLoad.getString(IO_LOG_CONFIG_HOST)
  private val ioLogPort = configLoad.getString(IO_LOG_CONFIG_PORT)
  private val IO_SERVICE_URL: String = s"http://$ioLogHost:$ioLogPort"
  private val IO_SERVICE_LOG_ENDPOINT: String = IO_SERVICE_URL + "/io_log"
  private val config = RabbitMQConfig()
  private val queueName = config.queue.name
  private val iOLogPayloadBuilder = IOLogPayloadBuilder()
  private val jsonLog: String = iOLogPayloadBuilder.build

  Given("""^The service is up and running$"""){ () =>
    val response: HttpResponse[String] = Http(IO_SERVICE_URL).asString

    response.code shouldEqual 200
  }

  When("""^I receive a log in Json format$"""){ () =>
    val response: HttpResponse[String] = Http(IO_SERVICE_LOG_ENDPOINT)
                                                      .postData(jsonLog)
                                                      .header("content-type", "application/json")
                                                      .asString

    response.code shouldEqual 200
  }

  Then("""^I should send a log event to ingestion exchange$"""){ () =>
    var maybeLogEvent: Option[LogEvent] = None
    QueueHelper.getMessages(queueName, (m: String) => {
      maybeLogEvent = Some(Json.parse(m).as[LogEvent])
    })

    eventually (timeout(5 seconds), interval(5 millis)) {
      val actualLogEvent = maybeLogEvent.getOrElse(throw new AssertionError("No message received in the queue"))

      actualLogEvent.rawLog should include(iOLogPayloadBuilder.pe)
      actualLogEvent.rawLog should include(iOLogPayloadBuilder.mac)
      actualLogEvent.rawLog should include(iOLogPayloadBuilder.receivedAt)
      actualLogEvent.rawLog should include(iOLogPayloadBuilder.uuid)
    }
  }
}
