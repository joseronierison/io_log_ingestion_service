package unit.domain

import domain.LogEvent
import org.scalatest.{FlatSpec, Matchers}

class LogEventTest extends FlatSpec with Matchers {
  val A_PAYLOAD = """"{"any": "payload"}"""

  behavior of "building a log event"

  it should "evaluate a log event with provided payload" in {
    val logEvent = LogEvent.adapt(A_PAYLOAD)

    logEvent.rawLog shouldEqual A_PAYLOAD
  }

  it should "evaluate different traceId when function is called twice" in {
    LogEvent.adapt(A_PAYLOAD).traceId shouldNot equal(LogEvent.adapt(A_PAYLOAD).traceId)
  }

}
