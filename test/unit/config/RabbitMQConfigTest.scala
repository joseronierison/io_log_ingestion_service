package unit.config

import com.typesafe.config.{Config, ConfigFactory}
import config.RabbitMQConfig
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

class RabbitMQConfigTest extends FlatSpec with Matchers with MockitoSugar with BeforeAndAfterEach {
  private val config: Config = ConfigFactory.load()

  private val host = "localhost"
  private val port = 5672
  private val virtualHost = "/"
  private val exchangeName = "io_log_exchange_test"
  private val exchangeType = "direct"
  private val user = "rabbitmq"
  private val password = "rabbitmq"

  val queueName = "ingestion_queue_test"
  val queueRoutingKey = "io_log.ingestion"

  behavior of "Rabbit MQ Config"
  it should "get host config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.host shouldEqual host
  }

  it should "get port config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.port shouldEqual port
  }

  it should "get virtual host config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.virtualHost shouldEqual virtualHost
  }

  it should "get exchange name config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.exchange.name shouldEqual exchangeName
  }

  it should "get exchange config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.exchange.exchangeType shouldEqual exchangeType
  }

  it should "get queue with name from config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.queue.name shouldEqual queueName
  }

  it should "get queue with routing key from config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.queue.routingKey shouldEqual queueRoutingKey
  }

  it should "get user config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.username shouldEqual user
  }

  it should "get password config value" in {
    val rabbitMQConfig: RabbitMQConfig = RabbitMQConfig(config)

    rabbitMQConfig.password shouldEqual password
  }

}
