package unit.io_log_dispatcher

import common.builders.IOLogPayloadBuilder
import domain.LogEvent
import infrastructure.QueuePublisher
import io_log_dispatcher.{IOLogDispatcher, IoLogIngestionEventPublisher}
import org.mockito.Matchers._
import org.mockito.Mockito.{times, verify, when}
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import org.mockito.Mockito

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class IOLogDispatcherTest extends FlatSpec with Matchers with MockitoSugar with BeforeAndAfterAll {
  private val ioLogIngestionEventPublisherMock = mock[IoLogIngestionEventPublisher]

  val ioLog: String = IOLogPayloadBuilder().build
  val logEvent = LogEvent.adapt(ioLog)

  override def afterAll(): Unit = {
    Mockito.reset(ioLogIngestionEventPublisherMock)
  }

  behavior of "Dispatch IO Log"
  it should "create io log event and publish it" in {
    val result: Future[LogEvent] = Future {logEvent}
    when(ioLogIngestionEventPublisherMock.publish(any[LogEvent], any())).thenReturn(result)

    val actualResult = IOLogDispatcher.dispatch(ioLog, ioLogIngestionEventPublisherMock)

    eventually {
      verify(ioLogIngestionEventPublisherMock, times(1)).publish(any[LogEvent], any[QueuePublisher])

      actualResult.onComplete { publishResult =>
        publishResult.get shouldEqual logEvent
      }
    }
  }


}
