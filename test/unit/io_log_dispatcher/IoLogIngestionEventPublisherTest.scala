package unit.io_log_dispatcher

import common.builders.LogEventBuilder
import domain.LogEvent
import infrastructure.QueuePublisher
import infrastructure.parsing.LogEventReadersAndWriters
import io_log_dispatcher.IoLogIngestionEventPublisher
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import org.mockito.Matchers._
import org.mockito.Mockito
import org.scalatest.concurrent.Eventually.eventually
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class IoLogIngestionEventPublisherTest extends FlatSpec with MockitoSugar with Matchers with BeforeAndAfterAll with LogEventReadersAndWriters {
  private val queuePublisherMock = mock[QueuePublisher]
  private val logEvent: LogEvent = LogEventBuilder().build
  private val logEventExpectedJson: String = Json.toJson(logEvent).toString()

  override def afterAll(): Unit = {
    Mockito.reset(queuePublisherMock)
  }

  behavior of "publishing a log event"
  it should "serialized log event to json to publish" in {
    when(queuePublisherMock.publish(anyString())).thenReturn(Future {})

    val actualResult = IoLogIngestionEventPublisher.publish(logEvent, queuePublisherMock)

    eventually {
      verify(queuePublisherMock, times(1) ).publish(logEventExpectedJson)

      actualResult.onComplete { publishResult =>
        publishResult.get shouldEqual logEvent
      }
    }
  }
}
