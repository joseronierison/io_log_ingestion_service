package integration.io_log_dispatcher

import common.builders.IOLogPayloadBuilder
import domain.LogEvent
import infrastructure.parsing.{JodaDateTimeReadersAndWriters, LogEventReadersAndWriters}
import io_log_dispatcher.LogDispatcherController
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.Future
import scala.xml.XML.loadString

class LogDispatcherControllerTest extends FlatSpec with MockitoSugar with Matchers with BeforeAndAfterEach
  with LogEventReadersAndWriters {

  val controller = new LogDispatcherController()

  behavior of "receiving log json from a device"
  it should "dispatch io log" in {
    val iOLogPayloadBuilder = IOLogPayloadBuilder()
    val jsonString: String = iOLogPayloadBuilder.build

    val request = FakeRequest(POST, "/io_log").withJsonBody(Json.parse(jsonString))

    val result: Future[Result] = controller.add().apply(request)
    val bodyText: String = contentAsString(result)
    val actualLogEvent = Json.parse(bodyText).as[LogEvent]

    status(result) shouldEqual OK
    actualLogEvent.rawLog should include(iOLogPayloadBuilder.pe)
    actualLogEvent.rawLog should include(iOLogPayloadBuilder.mac)
    actualLogEvent.rawLog should include(iOLogPayloadBuilder.uuid)
    actualLogEvent.rawLog should include(iOLogPayloadBuilder.receivedAt)
  }

  behavior of "receiving text from a device"
  it should "dispatch io log" in {
    val ioLogText = "pe=a-pe-value,uuid=a-uuid,tim=a-date,mac=a-mac,record=1:2:3"

    val request = FakeRequest(POST, "/io_log").withTextBody(ioLogText)

    val result: Future[Result] = controller.add().apply(request)
    val bodyText: String = contentAsString(result)
    val actualLogEvent = Json.parse(bodyText).as[LogEvent]

    status(result) shouldEqual OK
    actualLogEvent.rawLog shouldEqual ioLogText
  }

  behavior of "receiving xml from a device"
  it should "dispatch io log" in {
    val ioLogXml =
      """<root>
        |<MAC>fc:01:7c:c0:42:37</MAC>
        |<PE>a-pe-value</PE>
        |<Record>
        | <element>1</element>
        | <element>3</element>
        | <element>2</element>
        | <element>5</element>
        | <element>6</element>
        |</Record>
        |<TIM>2018-07-22</TIM>
        |<UID>99ffdcc5-1f07-4251-b13e-aa6cd69765a2</UID>
        |</root>""".stripMargin

    val request = FakeRequest(POST, "/io_log").withXmlBody(loadString(ioLogXml))

    val result: Future[Result] = controller.add().apply(request)
    val bodyText: String = contentAsString(result)
    val actualLogEvent = Json.parse(bodyText).as[LogEvent]

    status(result) shouldEqual OK
    actualLogEvent.rawLog shouldEqual ioLogXml
  }
}
