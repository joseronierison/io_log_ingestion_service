package integration.infrastructure

import common.QueueHelper
import config.RabbitMQConfig
import infrastructure.QueuePublisher
import org.scalatest._
import org.scalatest.concurrent.Eventually.eventually
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global

class QueuePublisherTest extends AsyncFlatSpec with Matchers with BeforeAndAfterAll {
  private val config: RabbitMQConfig = RabbitMQConfig()
  private val queueName: String = config.queue.name
  private val logger: Logger = Logger(getClass)

  val message = """{"a": "json-message"}"""

  override def beforeAll(): Unit = {
    QueueHelper.reset
    QueueHelper.setupExchange
  }

  behavior of "Publishing a single message on the queue"
  it should "publish one message to the queue" in {
    QueuePublisher(config, logger).publish(message).map { _ =>
      QueueHelper.countMessages(queueName) shouldEqual 1
    }
  }

  it should "publish the provided message to the queue" in {
    QueuePublisher(config, logger).publish(message).map { _ =>
      var blocked: Option[Boolean] = None
      QueueHelper.getMessages(queueName, (m: String) => {
        blocked = Some(m.equals(message))
      })

      eventually { blocked shouldEqual Some(true) }
    }
  }

}
