package common.builders

import domain.LogEvent

case class LogEventBuilder(log: String = IOLogPayloadBuilder().build) {
  def build: LogEvent = {
    LogEvent.adapt(log)
  }
}
