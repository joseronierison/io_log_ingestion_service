package common

import com.rabbitmq.client.AMQP.{BasicProperties, Exchange, Queue}
import com.rabbitmq.client._
import config.RabbitMQConfig
import play.api.http.MimeTypes

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object QueueHelper {
  private val config: RabbitMQConfig = RabbitMQConfig()
  private val connection: Connection = connect()
  private val channel: Channel = connection.createChannel()

  def publish(message: String, routingKey: String): Future[Unit] = {
    val messageBodyBytes = message.getBytes

    val amqpProperties = new BasicProperties.Builder()
      .contentType(MimeTypes.JSON)
      .priority(1)
      .build()

    Future {
      channel.basicPublish(config.exchange.name, routingKey, amqpProperties, messageBodyBytes)
    }
  }

  def setupExchange: (Exchange.DeclareOk, Queue.DeclareOk, Queue.BindOk) = {
    (
      channel.exchangeDeclare(config.exchange.name, config.exchange.exchangeType, true),
      channel.queueDeclare(config.queue.name, true, false, false, null),
      channel.queueBind(config.queue.name, config.exchange.name, config.queue.routingKey)
    )
  }

  def countMessages(queueName: String): Long = channel.messageCount(queueName)

  def reset: (Exchange.DeleteOk, Queue.DeleteOk) = {
    (
      channel.exchangeDelete(config.exchange.name),
      channel.queueDelete(config.queue.name)
    )
  }

  def getMessages(queueName: String, callback: String => Unit): Unit = {
    channel.basicConsume(queueName, false, "a-tag",
      new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties,
                                  body: Array[Byte]): Unit = {
        callback(body.map(_.toChar).mkString)
        channel.basicAck(envelope.getDeliveryTag, false)
      }
    })
  }

  private def connect(): Connection = {
    val factory = new ConnectionFactory

    factory.setUsername(config.username)
    factory.setPassword(config.password)
    factory.setVirtualHost(config.virtualHost)
    factory.setHost(config.host)
    factory.setPort(config.port)

    factory.newConnection
  }
}
