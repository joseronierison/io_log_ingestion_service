
# Io Log Service

### Running

You need to download and install sbt for this application to run.

Once you have sbt installed, the following at the command prompt will start up Play in development mode:

Before run the application, setup their dependencies:

```
docker-compose up
```

```
sbt run
```

Play will start up on the HTTP port at http://localhost:9000/.   You don't need to deploy or reload anything -- changing any source code while the server is running will automatically recompile and hot-reload the application on the next HTTP request. 

### Testing

In order to run unit tests

```
sbt unit
```

In order to run integration tests

```
sbt integration
```

In order to run functional tests

```
sbt cucumber
```


