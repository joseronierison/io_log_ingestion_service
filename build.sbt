import sbt.Keys._

lazy val GatlingTestConf = config("gatling") extend Test
lazy val UnitTestConf = config("unit") extend Test
lazy val IntegrationTestConf = config("integration") extend Test

scalaVersion := "2.11.11"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

libraryDependencies ++= Seq(
  "com.netaporter" %% "scala-uri" % "0.4.14",
  "net.codingwell" %% "scala-guice" % "4.1.0",
  "com.rabbitmq" % "amqp-client" % "5.3.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test,
  "com.typesafe.play" %% "play-json-joda" % "2.6.0-RC1",
  "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.2.5" % Test,
  "io.gatling" % "gatling-test-framework" % "2.2.5" % Test,
  "org.mockito" % "mockito-all" % "1.9.5" % Test,
  "io.cucumber" %% "cucumber-scala" % "2.0.1" % Test,
  "org.scalaj" %% "scalaj-http" % "2.4.0" % Test
)

enablePlugins(CucumberPlugin)

CucumberPlugin.glue := "functional/steps"
CucumberPlugin.features := List("test/functional/features")

lazy val root = (project in file("."))
  .configs(IntegrationTestConf, UnitTestConf, GatlingTestConf)
  .enablePlugins(Common, PlayScala, GatlingPlugin)
  .settings(inConfig(UnitTestConf)(Defaults.testTasks): _*)
  .settings(inConfig(IntegrationTestConf)(Defaults.testTasks): _*)
  .settings(inConfig(GatlingTestConf)(Defaults.testSettings): _*)
  .settings(
    name := """io_log_ingestion_service""",
    scalaSource in GatlingTestConf := baseDirectory.value / "/gatling/simulation"
  )

coverageExcludedPackages := "<empty>;Module;.*Reverse*Controller*;router.*"

lazy val unit = TaskKey[Unit]("unit", "Runs all Unit Tests.")
lazy val integration = TaskKey[Unit]("integration", "Runs all Integration Tests.")

unit := (test in UnitTestConf).value
integration := (test in IntegrationTestConf).value

testOptions in UnitTestConf := Seq(Tests.Filter(testPackageName => testPackageName.startsWith("unit")))
javaOptions in UnitTestConf += s"-Dconfig.file=${baseDirectory.value}/conf/application-testlocal.conf"

testOptions in IntegrationTestConf := Seq(Tests.Filter(testPackageName => testPackageName.startsWith("integration")))
javaOptions in IntegrationTestConf += s"-Dconfig.file=${baseDirectory.value}/conf/application-integrationtests.conf"
