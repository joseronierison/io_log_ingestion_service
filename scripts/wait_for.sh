#!/usr/bin/env bash

HOST=$1
PORT=$2

while ! curl -i http://$HOST:$PORT/api/overview; do
    sleep 1;
done