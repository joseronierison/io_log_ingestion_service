package config

import com.typesafe.config.{Config, ConfigFactory}


sealed case class Exchange(name: String, exchangeType: String)
sealed case class Queue(name: String, routingKey: String)
sealed case class RabbitMQConfig(
  host: String,
  port: Int,
  virtualHost: String,
  exchange: Exchange,
  queue: Queue,
  username: String,
  password: String
) {
  override def toString: String = s"RabbitMQConfig(host = $host, port = $port, virtualHost = $virtualHost)"
}

object RabbitMQConfig {
  private val RABBIT_MQ_CONFIG_PREFIX = "rabbitmq"
  private val HOST = s"$RABBIT_MQ_CONFIG_PREFIX.host"
  private val PORT = s"$RABBIT_MQ_CONFIG_PREFIX.port"
  private val VIRTUAL_HOST = s"$RABBIT_MQ_CONFIG_PREFIX.virtualHost"
  private val EXCHANGE_NAME = s"$RABBIT_MQ_CONFIG_PREFIX.exchange.name"
  private val EXCHANGE_TYPE = s"$RABBIT_MQ_CONFIG_PREFIX.exchange.type"
  private val QUEUE_NAME = s"$RABBIT_MQ_CONFIG_PREFIX.queue.name"
  private val QUEUE_ROUTING_KEY = s"$RABBIT_MQ_CONFIG_PREFIX.queue.routingKey"
  private val USER = s"$RABBIT_MQ_CONFIG_PREFIX.username"
  private val PASSWORD = s"$RABBIT_MQ_CONFIG_PREFIX.password"

  def apply(config: Config = ConfigFactory.load()): RabbitMQConfig = {
    RabbitMQConfig(
      host = config.getString(HOST),
      port = config.getInt(PORT),
      virtualHost = config.getString(VIRTUAL_HOST),
      exchange = Exchange(config.getString(EXCHANGE_NAME), config.getString(EXCHANGE_TYPE)),
      queue = Queue(config.getString(QUEUE_NAME), config.getString(QUEUE_ROUTING_KEY)),
      username = config.getString(USER),
      password = config.getString(PASSWORD)
    )
  }

}
