package io_log_dispatcher

import config.RabbitMQConfig
import domain.LogEvent
import infrastructure.QueuePublisher
import infrastructure.parsing.LogEventReadersAndWriters
import io_log_dispatcher.IoLogIngestionEventPublisher.{config, logger}
import play.api.Logger
import play.api.libs.json.Json

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

trait IoLogIngestionEventPublisher {
  def publish(logEvent: LogEvent, queuePublisher: QueuePublisher = QueuePublisher(config, logger)): Future[LogEvent]
}

object IoLogIngestionEventPublisher extends IoLogIngestionEventPublisher with LogEventReadersAndWriters {
  val config: RabbitMQConfig = RabbitMQConfig()
  val logger: Logger = Logger(getClass)

  def publish(logEvent: LogEvent, queuePublisher: QueuePublisher = QueuePublisher(config, logger)): Future[LogEvent] = {
    val message = Json.toJson(logEvent).toString()

    queuePublisher.publish(message).map(_ => logEvent)
  }
}
