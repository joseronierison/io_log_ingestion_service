package io_log_dispatcher

import domain.LogEvent
import infrastructure.parsing.LogEventReadersAndWriters
import play.api.Logger
import play.api.http.HttpVerbs
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class LogDispatcherController extends Controller with HttpVerbs with LogEventReadersAndWriters {
  val logger = Logger(getClass)

  def add: Action[AnyContent] = Action.async { implicit request =>
    request.body match {
      case AnyContentAsText(payload) =>
        logger.info(s"Received text message '$payload'")
        dispatch(payload)
      case AnyContentAsXml(payload) =>
        logger.info(s"Received xml message '$payload'")
        dispatch(payload.toString)
      case AnyContentAsJson(payload) =>
        logger.info(s"Received json message '$payload'")
        dispatch(payload.toString)
      case _ => handleBadRequest(request.body)
    }
  }

  private def dispatch(payload: String): Future[Result] = {
    IOLogDispatcher.dispatch(payload).map { logEvent: LogEvent =>
      val ioLogJson = Json.toJson(logEvent).toString
      Ok(ioLogJson)
    }
  }

  private def handleBadRequest(body: Any): Future[Result] = Future {
    logger.error(s"Error handling message $body")
    BadRequest("It is a invalid request, so the log was not ingested. Please, try again with a proper request.")
  }
}
