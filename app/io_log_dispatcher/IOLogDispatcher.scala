package io_log_dispatcher

import domain.LogEvent
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Failure

trait IOLogDispatcher {
  def dispatch(ioLog: String, publisher: IoLogIngestionEventPublisher): Future[LogEvent]
}

object IOLogDispatcher extends IOLogDispatcher {
  private val logger: Logger = Logger(getClass)

  def dispatch(ioLog: String, publisher: IoLogIngestionEventPublisher = IoLogIngestionEventPublisher): Future[LogEvent] = {
    val logEvent = LogEvent.adapt(ioLog)
    publisher
      .publish(logEvent)
      .andThen {
        case Failure(e) => {
          logger.error(s"There was error publishing the log '$logEvent', caused by '${e.getMessage}'. $e")
        }
      }
  }
}
