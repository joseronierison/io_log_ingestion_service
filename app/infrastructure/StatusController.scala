package infrastructure

import play.api.http.HttpVerbs
import play.api.mvc.{Action, AnyContent, Controller}

class StatusController extends Controller with HttpVerbs {
  def index = Action {
    Ok("Server is up and running!")
  }
}
