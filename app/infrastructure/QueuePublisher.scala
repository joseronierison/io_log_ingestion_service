package infrastructure

import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import config.RabbitMQConfig
import play.api.Logger
import play.api.http.MimeTypes
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class QueuePublisher(config: RabbitMQConfig, logger: Logger) {
  def publish(message: String): Future[Unit] = {
    for {
      channel <- connectToChannel()
      _ <- setupExchange(channel)
      _ <- setupQueue(channel)
      _ <- bindQueueToExchange(channel)
      publishResult <- publishJsonMessage(message)(channel)
    } yield publishResult
  }

  private def publishJsonMessage(message: String)(channel: Channel): Future[Unit] = {
    val messageBodyBytes: Array[Byte] = message.getBytes

    val amqpProperties = new BasicProperties.Builder()
      .contentType(MimeTypes.JSON)
      .priority(1)
      .build()

    Future {
      logger.info(s"Sending json message '$message' to exchange '${config.exchange.name}' with routing key '${config.queue.routingKey}'")
      channel.basicPublish(config.exchange.name, config.queue.routingKey, amqpProperties, messageBodyBytes)
    }
  }

  private def connect() : Future[Connection] = {
    Future {

      val factory = new ConnectionFactory

      factory.setUsername(config.username)
      factory.setPassword(config.password)
      factory.setVirtualHost(config.virtualHost)
      factory.setHost(config.host)
      factory.setPort(config.port)

      logger.info(s"Attempting to connect to '${config.host}:${config.port}', virtual host '${config.virtualHost}'")

      factory.newConnection
    }
  }

  private def connectToChannel(): Future[Channel] = connect().map(_.createChannel())

  private def setupExchange(channel: Channel) = Future {
    logger.info(s"Setting up exchange '${config.exchange.name}' with type '${config.exchange.exchangeType}'")
    channel.exchangeDeclare(config.exchange.name, config.exchange.exchangeType, true)
  }

  private def setupQueue(channel: Channel) = Future {
    logger.info(s"Setting up queue '${config.queue.name}'")
    channel.queueDeclare(config.queue.name, true, false, false, null)
  }

  private def bindQueueToExchange(channel: Channel) = Future {
    logger.info(s"Binding '${config.queue.name}' to exchange '${config.exchange.name}' with routing key '${config.queue.routingKey}'")
    channel.queueBind(config.queue.name, config.exchange.name, config.queue.routingKey)
  }
}

object QueuePublisher {
  def apply(config: RabbitMQConfig, logger: Logger): QueuePublisher = new QueuePublisher(config, logger)
}