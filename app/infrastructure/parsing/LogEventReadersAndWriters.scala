package infrastructure.parsing

import domain.LogEvent
import play.api.libs.json.{Json, Reads, Writes}

trait LogEventReadersAndWriters extends JodaDateTimeReadersAndWriters {
  implicit val implicitReaders: Reads[LogEvent] = Json.reads[LogEvent]
  implicit val implicitWriters: Writes[LogEvent] = Json.writes[LogEvent]
}
