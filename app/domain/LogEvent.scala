package domain

import org.joda.time.DateTime
import java.util.UUID.randomUUID

case class LogEvent(
  traceId: String,
  deviceId: String,
  rawLog: String,
  receivedAt: DateTime
)

object LogEvent {
  private val TEMP_DEVICEID = "a-device-id"

  def adapt(log: String): LogEvent = {
    val traceId: String = randomUUID().toString

    LogEvent(
      traceId = traceId,
      deviceId = TEMP_DEVICEID,
      rawLog = log,
      receivedAt = DateTime.now()
    )
  }
}

