package gatling.simultion

import common.builders.IOLogPayloadBuilder
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.language.postfixOps

class GatlingTest extends Simulation {

  val httpConf = http
    .baseURL("http://localhost:9000")
    .contentTypeHeader("application/json")


  val scn = scenario("IO Log Ingestion")
    .exec(http("ingest-io-log")
    .post("/io_log")
    .body(StringBody(IOLogPayloadBuilder().build))
    .check(status.is(200)))
    .pause(7)


  setUp(scn.inject(rampUsers(100) over (60 seconds))
    .throttle(reachRps(10000) in (2 minute),
      holdFor(3 minutes))
    .protocols(httpConf))
}